package com.serc.rt168.jms.adpater;

import java.net.UnknownHostException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;
import com.serc.rt168.jms.listener.ConsumerListener;

public class ConsumerAdapter {
	private static Logger logger = LogManager.getLogger(ConsumerAdapter.class.getName());
	
	public void sendToMongo(String json) throws UnknownHostException {
		MongoClient client = new MongoClient();
		DB db = client.getDB("vendor");
		DBCollection collection  = db.getCollection("contact");
		logger.info("Converting JSON to DbObject");
		DBObject object = (DBObject)JSON.parse(json);
		collection.insert(object);
		logger.info("Sent To MongoDB");
		
		
	}

}
