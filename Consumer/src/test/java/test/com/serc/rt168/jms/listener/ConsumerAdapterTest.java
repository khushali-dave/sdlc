package test.com.serc.rt168.jms.listener;

import static org.junit.Assert.*;

import java.net.UnknownHostException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.serc.rt168.jms.adpater.ConsumerAdapter;

public class ConsumerAdapterTest {
	String json ="{vendorName:\"Microsoft\",firstName:\"Bob\",lastName:\"Smith\",address:\"123 Main St\",city:\"Tulsa\",state:\"OK\",zip:\"71345\",email:\"Bob@microsoft.com\",phoneNumber:\"734-123-4567\"}";

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSendToMongo() throws UnknownHostException{
		ConsumerAdapter adapter = new ConsumerAdapter();
		adapter.sendToMongo(json);
		assertNotNull(json);
	}

}
